package repository

import (
	"CrudappV2/internal/pkg/domain"
	"github.com/jmoiron/sqlx"
)


type Auth interface {
	CreateUser(user domain.User) (int, error)
	GetUser(email, password string) (domain.User, error)
}


type Paste interface {
	CreatePaste(userId int, token, name, text string) (int, error)
	UpdatePaste(id, userID int) error
	DeletePaste(id, userID int) error
	GetPaste(id, UserID int) (domain.Paste, error)
	GetAllPastes(UserID int) ([]domain.Paste, error)
}


type Link interface {
	CreateLink(userId int, token, longLink string) (int, error)
	UpdateLink(id, userID int) error
	DeleteLink(id, userID int) error
	GetLink(id, userID int) (domain.Link, error)
	GetAllLinks() ([]domain.Link, error)
}


type Repository struct {
	Auth
	Paste
	Link
}


func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		Auth:  NewAuthPostgresRepo(db),
		Paste: NewPastePostgresRepo(db),
		Link:  NewLinkPostgresRepo(db),
	}
}