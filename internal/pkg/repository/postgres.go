package repository

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	_ "github.com/jackc/pgx/stdlib"
)

const (
	usersTable 	= "users"
	linksTable 	= "links"
	pastesTable = "pastes"
)

type Config struct {
	Host 	 	string
	Port 	 	string
	Username 	string
	Password 	string
	DBName	    string
	SSLMode     string
	SSLCertPath string
}

func NewPostgresDB(cfg Config) *sqlx.DB {
	db, err := sqlx.Open("pgx",
		fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s sslrootcert=%s",
			cfg.Host,
			cfg.Port,
			cfg.Username,
			cfg.DBName,
			cfg.Password,
			cfg.SSLMode,
			cfg.SSLCertPath,
		))
	if err != nil {
		logrus.Fatal("Can't parse database config ", err)
	}

	// Check connection
	err = db.Ping()
	if err != nil {
		logrus.Fatal("can't connect to database ", err)
	}

	db.SetMaxOpenConns(50)

	return db
}