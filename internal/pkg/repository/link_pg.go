package repository

import (
	"CrudappV2/internal/pkg/domain"
	"github.com/jmoiron/sqlx"
)


type LinkPostgresRepo struct {
	db 	*sqlx.DB
}


func NewLinkPostgresRepo(db *sqlx.DB) *LinkPostgresRepo {
	return &LinkPostgresRepo{db: db}
}


func (r *LinkPostgresRepo) CreateLink(userId int, token, longLink string) (int, error) {
	return 0, nil
}


func (r *LinkPostgresRepo) UpdateLink(id, userID int) error {
	return nil
}


func (r *LinkPostgresRepo) DeleteLink(id, userID int) error {
	return nil
}


func (r *LinkPostgresRepo) GetLink(id, userID int) (domain.Link, error) {
	return domain.Link{}, nil
}


func (r *LinkPostgresRepo) GetAllLinks() ([]domain.Link, error) {
	return []domain.Link{}, nil
}