package repository

import (
	"CrudappV2/internal/pkg/domain"
	"github.com/jmoiron/sqlx"
)


type AuthPostgresRepo struct {
	db *sqlx.DB
}


func NewAuthPostgresRepo(db *sqlx.DB) *AuthPostgresRepo {
	return &AuthPostgresRepo{db: db}
}


func (r *AuthPostgresRepo) CreateUser(user domain.User) (int, error) {
	return 0, nil
}


func (r *AuthPostgresRepo) GetUser(email, password string) (domain.User, error) {
	return domain.User{}, nil
}