package repository

import (
	"CrudappV2/internal/pkg/domain"
	"github.com/jmoiron/sqlx"
)


type PastePostgresRepo struct {
	db *sqlx.DB
}


func NewPastePostgresRepo(db *sqlx.DB) *PastePostgresRepo {
	return &PastePostgresRepo{db: db}
}


func (r *PastePostgresRepo) CreatePaste(userId int, token, name, text string) (int, error) {
	return 0, nil
}


func (r *PastePostgresRepo) UpdatePaste(id, userID int) error {
	return nil
}


func (r *PastePostgresRepo) DeletePaste(id, userID int) error {
	return nil
}


func (r *PastePostgresRepo) GetPaste(id, UserID int) (domain.Paste, error) {
	return domain.Paste{}, nil
}


func (r *PastePostgresRepo) GetAllPastes(UserID int) ([]domain.Paste, error) {
	return []domain.Paste{}, nil
}