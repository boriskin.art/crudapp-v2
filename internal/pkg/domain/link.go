package domain

type Link struct {
	ID 			int
	UserID 		int
	Token 		string
	LongLink 	string
	ClicksCount int
}