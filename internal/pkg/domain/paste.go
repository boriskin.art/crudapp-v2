package domain

type Paste struct {
	ID 	   int
	UserID int
	Token  string
	Name   string
	Text   string
}
