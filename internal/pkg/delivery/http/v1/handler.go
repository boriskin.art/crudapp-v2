package v1

import (
	"CrudappV2/internal/pkg/service"
	"github.com/gorilla/mux"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}


func (h *Handler) InitRoutes() *mux.Router {
	// Main router
	router := mux.NewRouter()

	// API Main Router
	apiRouter := router.PathPrefix("/api/v1").Subrouter()

	// API AUTH Router
	authApiRouter := apiRouter.PathPrefix("/auth").Subrouter()
	authApiRouter.HandleFunc("/sign-up", h.signUp).Methods("POST")
	authApiRouter.HandleFunc("/sign-in", h.signIn).Methods("POST")

	// API PasteIt! Router
	pasteItApiRouter := apiRouter.PathPrefix("/pasteit").Subrouter()
	pasteItApiRouter.HandleFunc("/{id}", h.createPaste).Methods("POST")
	pasteItApiRouter.HandleFunc("/{id}", h.updatePaste).Methods("PUT")
	pasteItApiRouter.HandleFunc("/{id}", h.deletePaste).Methods("DELETE")
	pasteItApiRouter.HandleFunc("/{id}", h.getPaste).Methods("GET")
	pasteItApiRouter.HandleFunc("/{id}/list", h.getAllPastes).Methods("GET")

	// API ShortIt! Router
	shortItApiRouter := apiRouter.PathPrefix("/shortit").Subrouter()
	shortItApiRouter.HandleFunc("/{id}", h.createLink).Methods("POST")
	shortItApiRouter.HandleFunc("/{id}", h.updateLink).Methods("PUT")
	shortItApiRouter.HandleFunc("/{id}", h.deleteLink).Methods("DELETE")
	shortItApiRouter.HandleFunc("/{id}", h.getLink).Methods("GET")
	shortItApiRouter.HandleFunc("/{id}/list", h.getAllLinks).Methods("GET")


	return router
}