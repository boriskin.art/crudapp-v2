package service

import "CrudappV2/internal/pkg/repository"

type PasteService struct {
	r repository.Paste
}

func NewPasteService(r repository.Paste) *PasteService {
	return &PasteService{r: r}
}