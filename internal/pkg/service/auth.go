package service

import "CrudappV2/internal/pkg/repository"

type AuthService struct {
	r repository.Auth
}

func NewAuthService(r repository.Auth) *AuthService {
	return &AuthService{r: r}
}