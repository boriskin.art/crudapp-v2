package service

import "CrudappV2/internal/pkg/repository"

type Auth interface {

}

type Paste interface {

}

type Link interface {

}

type Service struct {
	Auth
	Paste
	Link
}

func NewService(r *repository.Repository) *Service {
	return &Service{
		Auth: 	NewAuthService(r.Auth),
		Paste:  NewPasteService(r.Paste),
		Link:   NewLinkService(r.Link),
	}
}