package service

import "CrudappV2/internal/pkg/repository"

type LinkService struct {
	r repository.Link
}

func NewLinkService(r repository.Link) *LinkService {
	return &LinkService{r: r}
}