package logger

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"os"
)

func InitLogger() {
	if viper.GetString("environment") != "dev" {
		logrus.SetFormatter(&logrus.JSONFormatter{})
	}

	logrus.SetReportCaller(true)
	logrus.SetOutput(os.Stdout)

	logrus.SetLevel(logrus.InfoLevel)
}