package main

import (
	"CrudappV2/configs"
	"CrudappV2/internal/pkg/delivery/http/v1"
	"CrudappV2/internal/pkg/repository"
	"CrudappV2/internal/pkg/service"
	"CrudappV2/pkg/logger"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"net/http"
)

// Main func
func main() {
	// Configs init
	configs.InitConfigs()

	// Logger init
	logger.InitLogger()

	db := repository.NewPostgresDB(repository.Config{
		Host:        viper.GetString("db.host"),
		Port:        viper.GetString("db.port"),
		Username:    viper.GetString("db.user"),
		Password:    viper.GetString("db.password"),
		DBName:      viper.GetString("db.dbname"),
		SSLMode:     viper.GetString("db.sslmode"),
		SSLCertPath: viper.GetString("db.sslrootcert"),
	})

	repos := repository.NewRepository(db)
	services := service.NewService(repos)
	handlers := v1.NewHandler(services)

	router := handlers.InitRoutes()

	// Start server
	logrus.Info("starting server", " type ", "START", " addr ", "http://" + viper.GetString("domain"))
	logrus.Fatal(http.ListenAndServe(viper.GetString("port"), router))
}