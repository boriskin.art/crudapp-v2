package configs

import (
	"flag"
	"github.com/spf13/viper"
	"log"
	"os"
)

func InitConfigs() {
	parseConfigs()

	var addr string
	var domain string

	flag.StringVar(&addr, "addr", viper.GetString("port"), "Change server address")
	flag.StringVar(&domain, "domain", viper.GetString("domain"), "Change site domain")
	flag.Parse()

	viper.Set("port", addr)
	viper.Set("domain", domain)
}

func parseConfigs() {
	viper.AddConfigPath("./configs")
	viper.SetConfigName("configs")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal(err)
	}

	viper.Set("db.password", os.Getenv("DB_PASSWORD"))
}
